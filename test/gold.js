const gold = artifacts.require('gold.sol');
const silver = artifacts.require('Silver.sol');

const { increaseTimeTo, duration } = require('openzeppelin-solidity/test/helpers/increaseTime');
const { latestTime } = require('openzeppelin-solidity/test/helpers/latestTime');

var Web3 = require("web3");
var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

var Web3Utils = require('web3-utils');

contract('CXN Contract', async (accounts) => {


    it('Should correctly initialize constructor of Gold token Contract', async () => {

        this.tokenhold = await gold.new(1000000,{ gas: 600000000 });

    });

    it('Should correctly initialize constructor of Silver token Contract', async () => {

        this.tokenhold = await Silver.new(1000000,{ gas: 600000000 });

    });    

    it('Should check a name of a token', async () => {

        let name = await this.tokenhold.name.call();
        assert.equal(name, "Gold");

    });

    it('Should check a symbol of a token', async () => {

        let symbol = await this.tokenhold.symbol.call();
        assert.equal(symbol, "Gold");

    });

    it('Should check a name of a token', async () => {

        let name = await this.tokenhold.name.call();
        assert.equal(name, "Silver");

    });

    it('Should check a symbol of a token', async () => {

        let symbol = await this.tokenhold.symbol.call();
        assert.equal(symbol, "Silver");

    });    

    it('Should check a decimal of a token', async () => {

        let decimals = await this.tokenhold.decimals.call();
        assert.equal(decimals, 18);

    });

    it('Should check a balance of a token contract', async () => {

        let owner = await this.tokenhold.balanceOf.call(this.tokenhold.address);
        assert.equal(owner.toNumber()/10**18,0);

    });

    it('Should check a balance of a owner contract', async () => {

        let owner = await this.tokenhold.balanceOf.call(accounts[0]);
        assert.equal(owner.toString(),'0');

    });

    it('Should check a total supply of a contract', async () => {

        let owner = await this.tokenhold.totalSupply();
        assert.equal(owner.toString(),'0');

    });

    it("should check approval by accounts 4 to accounts 2", async () => {

        let allowance = await this.tokenhold.allowance.call(accounts[4], accounts[2]);
        assert.equal(allowance, 0, "allowance is wrong when approve");

    });

    it("should Approve accounts[4] to spend specific tokens of accounts[2]", async () => {

        this.tokenhold.approve(accounts[4], web3.utils.toHex(50 * 10 ** 18), { from: accounts[2] });

    });

    it("should increase Approve accounts[4] to spend specific tokens of accounts[2]", async () => {

        this.tokenhold.increaseAllowance(accounts[4], web3.utils.toHex(50 * 10 ** 18), { from: accounts[2] });

    });

    it("should decrease Approve accounts[4] to spend specific tokens of accounts[2]", async () => {

        this.tokenhold.decreaseAllowance(accounts[4], web3.utils.toHex(50 * 10 ** 18), { from: accounts[2] });

    });

    it("should Approve accounts[4] to spend specific tokens of accounts[2]", async () => {

        let allowanceLater = await this.tokenhold.allowance.call(accounts[2], accounts[4]);
        assert.equal(allowanceLater, 50 * 10 ** 18, "allowance is wrong when approve");

    });

    it('Should be able to transfer tokens by owner', async () => {

        await this.tokenhold.transfer(accounts[1],web3.utils.toHex(100 * 10 ** 18), {from : accounts[0]});

    });

    it('Should check a balance of a token Receiver', async () => {

        let owner = await this.tokenhold.balanceOf.call(accounts[1]);
        assert.equal(owner.toString(),'100000000000000000000');

    });

    it('Should be able to transfer tokens by owner', async () => {

        await this.tokenhold.transfer(accounts[4],web3.utils.toHex(100 * 10 ** 18), {from : accounts[0]});

    });

    it('Should check a balance of a token Receiver', async () => {

        let owner = await this.tokenhold.balanceOf.call(accounts[4]);
        assert.equal(owner.toString(),'100000000000000000000');

    });

    it("should be able to transferfrom accounts[4] to accounts[1]", async () => {

        //this.tokenhold.transferFrom(accounts[4],accounts[2],web3.utils.toHex(50 * 10 ** 18),{from: accounts[1] });

    });

    it('Should check a balance of a beneficiary of accounts[1]', async () => {

        let owner = await this.tokenhold.balanceOf.call(accounts[1]);
        assert.equal(owner.toNumber(),100000000000);

    });

    it('Should be able to transfer tokens by previous locked account', async () => {

        await this.tokenhold.transfer(accounts[4],web3.utils.toHex(100 * 10 ** 9), {from : accounts[1]});

    });

    it('Should check a balance of a beneficiary of accounts[4] after sending all tokens', async () => {

        let owner = await this.tokenhold.balanceOf.call(accounts[4]);
        assert.equal(owner.toNumber(),100000000000);

    });

    it('Should check a balance of a sender accounts[1]', async () => {

        let owner = await this.tokenhold.balanceOf.call(accounts[1]);
        assert.equal(owner.toNumber(),0);

    });

    it("should check approval by accounts 4 to accounts 1 to spend tokens on the behalf of accounts 4", async () => {

        let allowance = await this.tokenhold.allowance.call(accounts[4], accounts[1]);
        assert.equal(allowance, 0, "allowance is wrong when approve");

    });

    it("should Approve accounts[1] to spend specific tokens of accounts[4]", async () => {

        this.tokenhold.approve(accounts[1], web3.utils.toHex(50 * 10 ** 9), { from: accounts[4] });

    });

    it("should check approval by accounts 4 to accounts 1 to spend tokens on the behalf of accounts 4", async () => {

        let allowance = await this.tokenhold.allowance.call(accounts[4], accounts[1]);
        assert.equal(allowance, 50000000000, "allowance is wrong when approve");

    });

    it("should increase Approve accounts[4] to spend specific tokens of accounts[1]", async () => {

        this.tokenhold.increaseAllowance(accounts[1], web3.utils.toHex(50 * 10 ** 9), { from: accounts[4] });

    });

    it("should check approval by accounts 4 to accounts 1 to spend tokens on the behalf of accounts 4", async () => {

        let allowance = await this.tokenhold.allowance.call(accounts[4], accounts[1]);
        assert.equal(allowance, 100000000000, "allowance is wrong when approve");

    });    

    it("should decrease Approve accounts[4] to spend specific tokens of accounts[1]", async () => {

        this.tokenhold.decreaseAllowance(accounts[1], web3.utils.toHex(50 * 10 ** 9), { from: accounts[4] });

    });

    it("should check approval by accounts 4 to accounts 1 to spend tokens on the behalf of accounts 4", async () => {

        let allowance = await this.tokenhold.allowance.call(accounts[4], accounts[1]);
        assert.equal(allowance, 50000000000, "allowance is wrong when approve");

    });

    it("should decrease Approve accounts[4] to spend specific tokens of accounts[1]", async () => {

        this.tokenhold.decreaseAllowance(accounts[1], web3.utils.toHex(500 * 10 ** 9), { from: accounts[4] });

    });    

    it("should check approval by accounts 4 to accounts 1 to spend tokens on the behalf of accounts 4", async () => {

        let allowance = await this.tokenhold.allowance.call(accounts[4], accounts[1]);
        assert.equal(allowance, 0, "allowance is wrong when approve");

    });

    it("should Approve accounts[1] to spend specific tokens of accounts[4] agin", async () => {

        this.tokenhold.approve(accounts[1], web3.utils.toHex(50 * 10 ** 9), { from: accounts[4] });

    });

    it("should check approval by accounts 4 to accounts 1 to spend tokens on the behalf of accounts 4", async () => {

        let allowance = await this.tokenhold.allowance.call(accounts[4], accounts[1]);
        assert.equal(allowance, 50000000000, "allowance is wrong when approve");

    });

    it("should be able to transferfrom accounts[4] to accounts[1]", async () => {

        this.tokenhold.transferFrom(accounts[4],accounts[1],web3.utils.toHex(50000000000),{from: accounts[1] });

    });

    it("should check approval by accounts 4 to accounts 1 to spend tokens on the behalf of accounts 4", async () => {

        let allowance = await this.tokenhold.allowance.call(accounts[4], accounts[1]);
        assert.equal(allowance, 0, "allowance is wrong when approve");

    });


    it('Should check a balance of a beneficiary of accounts[4] after sending all tokens', async () => {

        let owner = await this.tokenhold.balanceOf.call(accounts[4]);
        assert.equal(owner.toNumber(),50000000000);

    });

    it('Should check a balance of a receiver accounts[1]', async () => {

        let owner = await this.tokenhold.balanceOf.call(accounts[1]);
        assert.equal(owner.toNumber(),50000000000);

    });

    it('Should check a owner of a token before transferring ownership', async () => {

        let owner = await this.tokenhold.owner.call();
        assert.equal(owner, accounts[0]);

    });

    it('Should not be able to transfer ownership before ', async () => {

       try {
        await this.tokenhold.transferOwnership(accounts[9], {from : accounts[9]});
        } catch (error) {
            var error_ = 'Returned error: VM Exception while processing transaction: revert';
            assert.equal(error.message, error_, 'Reverted ');
        }

    });

    it('Should be able to transfer ownership before ', async () => {

      await this.tokenhold.transferOwnership(accounts[9]);

    });

    it('Should be able to accept transfer ownership before ', async () => {

      await this.tokenhold.acceptOwnership({from : accounts[9]});

    });

    it('Should check a owner of a token after transferring ownership', async () => {

        let owner = await this.tokenhold.owner.call();
        assert.equal(owner, accounts[9]);

    });

    it('Should be able to transfer ownership again to accounts[0] ', async () => {

      await this.tokenhold.transferOwnership(accounts[0], {from : accounts[9]});

    });

    it('Should be able to accept transfer ownership before ', async () => {

      await this.tokenhold.acceptOwnership({from : accounts[0]});

    });

    it('Should check a owner of a token after transferring ownership', async () => {

        let owner = await this.tokenhold.owner.call();
        assert.equal(owner, accounts[0]);

    });

    it('Should Not be able to send token to otc balance higher then user have', async () => {

        try {
        await this.tokenhold.sendTokens(accounts[2],web3.utils.toHex(10 * 10 ** 18),1, {from : accounts[0]});
            } catch (error) {
                var error_ = 'Returned error: VM Exception while processing transaction: revert SafeMath: multiplication overflow -- Reason given: SafeMath: multiplication overflow.';
                assert.equal(error.message, error_, 'Reverted ');
            }
    });

    it('Should check a balance of a owner after sending  tokens', async () => {

        let owner = await this.tokenhold.balanceOf.call(accounts[0]);
        assert.equal(owner.toNumber(),2999750000000000);

    });


    it('Should check a balance of a token reciever', async () => {

        let owner = await this.tokenhold.balanceOf.call(accounts[5]);
        assert.equal(owner.toNumber(),100000000000);

    });

    it('Should check a balance of a accounts[6]', async () => {

        let owner = await this.tokenhold.balanceOf.call(accounts[6]);
        assert.equal(owner.toNumber(),0);

    });

    it("should check approval by accounts 5 to accounts 6 to spend locked tokens on the behalf of accounts 5", async () => {

        let allowance = await this.tokenhold.allowance.call(accounts[5], accounts[6]);
        assert.equal(allowance, 0, "allowance is wrong when approve");

    });

    it("should Approve accounts[1] to spend specific tokens of accounts[4]", async () => {

        this.tokenhold.approve(accounts[6], web3.utils.toHex(50 * 10 ** 9), { from: accounts[5] });

    });

    it("should check approval by accounts 4 to accounts 1 to spend tokens on the behalf of accounts 4", async () => {

        let allowance = await this.tokenhold.allowance.call(accounts[5], accounts[6]);
        assert.equal(allowance, 50000000000, "allowance is wrong when approve");

    });


})




